package com.easynetty.core.net.entity;

public enum Command {
    TASK, FILE, STATUS, CONFIG, MESSAGE
}
